const pinataSDK = require('@pinata/sdk');
require('dotenv').config() // Store environment-specific variable from '.env' to process.env
const fs = require('fs')

const pinata = pinataSDK(
  process.env.PINATA_API_KEY,
  process.env.PINATA_SECRET_API_KEY
);

//await pinata.pinJSONToIPFS(body);
// or
const filename = process.argv[2]
console.log('Attempting to pin ', filename, ' to IPFS via pinata')
const rs = fs.createReadStream(filename);
pinata.pinFromFS(filename).then(result => console.log({result})).catch(err => console.error({err}))


//const FormData = require("form-data");
//const rfs = require("recursive-fs");
//const basePathConverter = require("base-path-converter");
//const JWT = process.env.PINATA_JWT
//
//const pinDirectoryToPinata = async () => {
//  const {got} = await import('got');
//  const url = `https://api.pinata.cloud/pinning/pinFileToIPFS`;
//  const src = process.argv[2];
//  var status = 0;
//  try {
//    const { dirs, files } = await rfs.read(src);
//    let data = new FormData();
//    for (const file of files) {
//      data.append(`file`, fs.createReadStream(file), {
//        filepath: basePathConverter(src, file),
//      });
//    }
//    const response = await got(url, {
//      method: 'POST',
//      headers: {
//        "Content-Type": `multipart/form-data; boundary=${data._boundary}`,
//        "Authorization": JWT
//      },
//      body: data
//    })
//    .on('uploadProgress', progress => {
//	console.log(progress);
//    });
//    console.log(JSON.parse(response.body));
//  } catch (error) {
//    console.log(error);
//  }
//};
//
//pinDirectoryToPinata()